package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {
    /**
     * 添加购物车
     * @param shoppingCart
     */
    @Insert("insert into shopping_cart (name, image, user_id, dish_id, setmeal_id, dish_flavor, amount, create_time) " +
            "values (#{name},#{image},#{userId},#{dishId},#{setmealId},#{dishFlavor},#{amount},#{createTime})")
    public void insert(ShoppingCart shoppingCart) ;

    /**
     * 条件查询购物车
     * @param shoppingCart
     */
    List<ShoppingCart> list(ShoppingCart shoppingCart);

    /**
     * 修改购物车
     * @param cart
     */
    void update(ShoppingCart cart);

    /**
     * 清空购物车
     * @param userid
     */
    @Delete("delete from shopping_cart  where user_id=#{userid}")
    void clean(Long userid);

    /**
     * 减少购物车
     * @param shoppingCart
     */
    void sub(ShoppingCart shoppingCart);
}

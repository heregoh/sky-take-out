package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.dto.GoodsSalesDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrderMapper {
    /**
     * 插入订单数据
     * @param orders
     */
     void insert(Orders orders);

    /**
     * 根据订单号查询订单
     * @param orderNumber
     */
    @Select("select * from orders where number = #{orderNumber}")
    Orders getByNumber(String orderNumber);

    /**
     * 修改订单信息
     * @param orders
     */
    void update(Orders orders);

    /**
     * 查询支付超时的订单
     * @param status
     * @param time
     */
    @Select("select * from orders where status =#{status}  and order_time<#{time}")
    List<Orders> getByStatusAndOrderTimeDl(Integer status, LocalDateTime time);

    /**
     * 订单分页查询
     * @param ordersPageQueryDTO
     * @return
     */
    Page<Orders> pageQuery(OrdersPageQueryDTO ordersPageQueryDTO);

    /**
     * 根据id查询订单
     * @param id
     * @return
     */
    @Select("select * from orders where id=#{id}")
    List<Orders> getById(Long id);

    /**
     * 根据map计算营业额总额
     * @param map
     * @return
     */
    Double sumByMap(Map map);

    /**
     * 动态条件计算订单数据
     * @param map
     */
    Integer countByMap(Map map);

    /**
     * 查询某一时间段销量的top10
     * @param beginTime
     * @param endTime
     * @return
     */
    List<GoodsSalesDTO> getSalesTop10(LocalDateTime beginTime, LocalDateTime endTime);

    /**
     * 按状态查询订单数量
     *
     * @return
     */
    @Select("select count(id) from orders where status =#{status}")
    Integer getOrderByStatus(Integer status);
}

package com.sky.mapper;

import com.sky.entity.AddressBook;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AddressBookMapper {

    @Insert("insert into address_book (user_id, consignee, sex, phone, province_code, province_name, city_code, city_name, district_code, district_name, detail, label) " +
            "values (#{userId},#{consignee},#{sex},#{phone},#{provinceCode},#{provinceName},#{cityCode},#{cityName},#{districtCode},#{districtName},#{detail},#{label})")
    void insert(AddressBook addressBook);

    @Select("select id, user_id, consignee, sex, phone, province_code, province_name, city_code, city_name, district_code, district_name, detail, label, is_default from address_book where user_id=#{userid}")
    List<AddressBook> getByUserid(Long userid);

    void update(AddressBook addressBook);

    @Select("select id, user_id, consignee, sex, phone, province_code, province_name, city_code, city_name, district_code, district_name, detail, label, is_default from address_book where id=#{id}")
    AddressBook getById(Long id);

    @Delete("delete from address_book where id =#{id}")
    void delete(Long id);

    void updateByUserId(AddressBook addressBook);

    /**
     * 获取默认地址
     * @return
     */
    @Select("select * from address_book where is_default=1")
    AddressBook getDefault();

}

package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealMapper setMealMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;
    /**
     * 新增套餐
     * @param setmealDTO
     */
    public void saveWithDish(SetmealDTO setmealDTO) {
        //新增套餐表
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setMealMapper.insert(setmeal);

        //新增套餐菜品表
        Long id = setmeal.getId(); //获取生成的套餐id
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        for (SetmealDish dish : setmealDishes) {
            dish.setSetmealId(id);
        }
        setmealDishMapper.insert(setmealDishes);
    }

    /**
     * 套餐分页查询
     * @param setmealPageQueryDTO
     * @return
     */
    public PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO) {
        PageHelper.startPage(setmealPageQueryDTO.getPage(),setmealPageQueryDTO.getPageSize());
        Page<Setmeal> page = setMealMapper.pageQuery(setmealPageQueryDTO);
        return new  PageResult(page.getTotal(),page.getResult());
    }

    /**
     * 套餐批量删除
     * @param ids
     */
    public void deleteBatch(List<Long> ids) {
        //判断是否可以删除-----判断套餐是否起售
        for (Long id : ids) {
            Setmeal setmeal = setMealMapper.getById(id);
            if(setmeal.getStatus()!=0){
                throw new DeletionNotAllowedException(MessageConstant.SETMEAL_ON_SALE);
            }
        }

        //删除套餐表数据
        setMealMapper.deleteBatch(ids);

        //删除套餐内菜品数据
        setmealDishMapper.deleteBatch(ids);
    }

    /**
     * 根据id查询套餐
     * @param id
     * @return
     */
    public SetmealDTO getById(Long id) {
        //根据id从套餐表查询数据
        Setmeal setmeal =  setMealMapper.getById(id);

        //根据id从套餐内菜品中查询数据
        List<SetmealDish> setmealDish = setmealDishMapper.getDishBySetmealId(id);

        //合并为最终数据
        SetmealDTO setmealDTO = new SetmealDTO();
        BeanUtils.copyProperties(setmeal,setmealDTO);
        setmealDTO.setSetmealDishes(setmealDish );
        return setmealDTO;
    }

    /**
     * 修改套餐
     *
     * @param setmealDTO
     */
    public void update(SetmealDTO setmealDTO) {
        //根据id修改套餐表
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setMealMapper.update(setmeal);

        //根据id删除套餐内菜品,然后插入数据
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        List<Long> ids = new ArrayList<>();
        ids.add(setmeal.getId());
        setmealDishMapper.deleteBatch(ids);
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(ids.get(0));
        }
        setmealDishMapper.insert(setmealDishes);
    }

    /**
     * 套餐起售或者停售
     * @param id
     * @param status
     */
    public void stopOrStart(Long id , int status) {
        Setmeal setmeal = new Setmeal();
        setmeal.setId(id);
        setmeal.setStatus(status);
        setMealMapper.update(setmeal);
    }

    /**
     * 根据分类查询套餐
     * @return
     */
    public List<Setmeal> list(Long categoryId) {
        List<Setmeal> setmealList = setMealMapper.list(categoryId);
        return setmealList;
    }

    /**
     * 根据id查询菜品选项
     * @param id
     * @return
     */
    public List<DishItemVO> getDishItemById(Long id) {
        return setMealMapper.getDishItemBySetmealId(id);
    }
}

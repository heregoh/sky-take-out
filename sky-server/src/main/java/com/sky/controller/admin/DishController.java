package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Category;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.service.impl.DishServiceImpl;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 菜品管理
 */
@Api(tags = "菜品管理")
@Slf4j
@RestController
@RequestMapping("/admin/dish")
public class DishController {
    @Autowired
    private DishService dishService;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 菜品上传
     *
     * @param dishDTO
     * @return
     */
    @PostMapping
    @ApiOperation("新增菜品")
    public Result save(@RequestBody DishDTO dishDTO) {
        log.info("菜品上传：{}", dishDTO);
        dishService.saveWithFlavor(dishDTO);

        //清除redis缓存
        String  key  = "dish_"+dishDTO.getCategoryId();
        redisTemplate.delete(key);
        return Result.success();
    }

    /**
     * 菜品分页查询
     *
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("菜品分页查询")
    public Result<PageResult> page(DishPageQueryDTO dishPageQueryDTO) {
        log.info("菜品分页查询");
        PageResult pageResult = dishService.pageQuery(dishPageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 删除批量菜品
     * @param ids
     * @return
     */
    @DeleteMapping
    @ApiOperation("删除批量菜品")
    public Result<String> delete(@RequestParam List<Long> ids) {
        log.info("删除批量菜品");
        dishService.deleteBatch(ids);

        //清除redis缓存---清除所有菜品缓存
        cleanCache("dish_*");
        return Result.success();
    }

    /**
     * 根据id查询菜品
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation("根据id查询菜品")
    public Result<DishVO> getById(@PathVariable  Long id) {
        log.info("根据id查询菜品");
        return Result.success(dishService.getDishWithFlavorsById(id));
    }

    /**
     * 根据分类查询菜品
     * @param categoryId
     * @return
     */
    @GetMapping ("/list")
    @ApiOperation("根据分类查询菜品")
    public Result<List<Dish>> list( Long categoryId){
        log.info("根据分类查询菜品：{}",categoryId);
        List<Dish> dishes =  dishService.list(categoryId);
        return Result.success(dishes);
    }

    /**
     * 修改菜品
     * @return
     */
    @PutMapping
    @ApiOperation("")
    public Result<String> update(@RequestBody DishDTO dishDTO){
        log.info("修改菜品:{}",dishDTO);
        dishService.updateWithFlavor(dishDTO);

        //清除redis缓存---清除所有菜品缓存
        cleanCache("dish_*");
        return Result.success();
    }

    /**
     * 起售或停售菜品
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/status/{status}")
    public Result stopOrStart(Long id , @PathVariable int status){
        log.info("起售或停售菜品,{},{}",id,status);
        dishService.stopOrStart(id,status);

        //清除redis缓存---清除当前菜品
        cleanCache("dish_*");
        return Result.success();
    }

    /**
     * 清楚缓存数据
     * @param pattern
     */
    private void  cleanCache(String pattern){
        Set keys = redisTemplate.keys(pattern);
        redisTemplate.delete(keys);
    }
}

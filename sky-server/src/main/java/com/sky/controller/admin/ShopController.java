package com.sky.controller.admin;

import com.sky.result.Result;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

@RestController("AdminShopController")
@RequestMapping("/admin/shop")
@Slf4j
public class ShopController {
    static final String Key = "SHOP_STATUS";
    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 查询店铺营业状态
     * @return
     */
    @GetMapping("/status")
    @ApiOperation("查询店铺营业状态")
    public Result<Integer> getStatus(){
        Integer status = (Integer) redisTemplate.opsForValue().get(Key);
        log.info("查询店铺营业状态为：{}",status==1 ? "营业中":"打烊中");
        return Result.success(status);
    }

    /**
     * 修改营业状态
     * @param status
     * @return
     */
    @PutMapping("/{status}")
    @ApiOperation("修改营业状态")
    public Result<String> setStatus(@PathVariable Integer status){
        log.info("修改营业状态:{}",status);
        redisTemplate.opsForValue().set(Key,status);
        return Result.success();
    }
}

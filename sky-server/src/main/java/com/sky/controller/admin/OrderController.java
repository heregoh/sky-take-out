package com.sky.controller.admin;

import com.sky.dto.OrdersDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.OrderDetail;
import com.sky.entity.Orders;
import com.sky.mapper.OrderDetailMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OrderService;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("AdminOrderController")
@RequestMapping("/admin/order")
@Api("订单管理")
@Slf4j
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderDetailMapper orderDetailMapper;

    /**
     * 条件查询订单
     *
     * @param ordersPageQueryDTO
     * @return
     */
    @GetMapping("/conditionSearch")
    @ApiOperation("条件查询订单")
    public Result<PageResult> OrderPage(OrdersPageQueryDTO ordersPageQueryDTO) {
        log.info("条件查询订单：{}", ordersPageQueryDTO);
        PageResult pageResult = orderService.pageQuery(ordersPageQueryDTO);
        return Result.success(pageResult);
    }


    /**
     * 根据order_id查询订单明细表
     *
     * @param order_id
     * @return
     */
    @GetMapping("/details/{order_id}")
    @ApiOperation("根据order_id查询订单明细表")
    public Result<OrderVO> OrderDetailPage(@PathVariable Long order_id) {
        log.info("根据order_id查询订单明细表:{}", order_id);
        OrderVO orderVO = orderService.getOrderVOById(order_id);

        return Result.success(orderVO);
    }

    /**
     * 接单
     * @param id
     * @return
     */
    @PutMapping("/confirm")
    @ApiOperation("接单")
    public Result orderConfirm(Long id){
        log.info("接单：{}",id);
//        orderService.orderConfirm(id);
        return Result.success();
    }

    /**
     * 接单
     * @param id
     * @return
     */
    @PutMapping ("/rejection")
    @ApiOperation("拒单")
    public Result orderRejection(Long id){
        log.info("拒单：{}",id);
//        orderService.orderConfirm(id);
        return Result.success();
    }

    /**
     * 按状态查询订单数量
     *
     * @return
     */
    @GetMapping("/statistics")
    public Result<OrderStatisticsVO> orderStatistics() {
        log.info("按状态查询订单数量");
        OrderStatisticsVO orderStatisticsVO =  orderService.getOrderStatistics();
        return Result.success(orderStatisticsVO);
    }
}

package com.sky.task;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class PoiTest {

    /**
     * 写入excel
     * @throws Exception
     */
    public static void write() throws Exception{
        //在内存中新建一个excel
        XSSFWorkbook excel = new XSSFWorkbook();
        //在excel中新建一个sheet
        XSSFSheet sheet = excel.createSheet();
        //在sheet中创建对象,ruwnum从0开始
        XSSFRow row = sheet.createRow(1);
        //创建单元格，写入内容
        row.createCell(1).setCellValue("姓名");
        row.createCell(2).setCellValue("城市");

        //创建新行，单元格，写入内容
        XSSFRow row1 = sheet.createRow(2);
        row1.createCell(1).setCellValue("张三");
        row1.createCell(2).setCellValue("北京");

        //创建新行，单元格，写入内容
        XSSFRow row2 = sheet.createRow(3);
        row2.createCell(1).setCellValue("李四");
        row2.createCell(2).setCellValue("上海");

        //通过输出流将excel写入磁盘
        FileOutputStream out = new FileOutputStream(new File("D:\\info.xlsx"));
        excel.write(out);

        //关闭资源
        out.close();
        excel.close();
    }

    /**
     *
     * 读取excel
     * @throws Exception
     */
    public static  void read() throws Exception{
        FileInputStream in = new FileInputStream(new File("D:\\info.xlsx"));
        //获取excel
        XSSFWorkbook excel = new XSSFWorkbook(in);
        //获取sheet
        XSSFSheet sheet = excel.getSheetAt(0);
        //获取有内容的最后一样行号
        int lastRowNum = sheet.getLastRowNum();

        //读取内容
        for (int i = 1; i <= lastRowNum; i++) {
            //获取行内容
            XSSFRow row = sheet.getRow(i);
            //获取单元格内容
            String stringCellValue1 = row.getCell(1).getStringCellValue();
            String stringCellValue2 = row.getCell(2).getStringCellValue();
            System.out.println(stringCellValue1+" "+stringCellValue2);
        }

    }
    public static void main(String[] args) throws Exception {
//        write();
//        read();
    }
}

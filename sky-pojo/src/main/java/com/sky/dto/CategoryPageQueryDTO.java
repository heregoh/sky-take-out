package com.sky.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "分类分页查询时传递数据的数据模型")
public class CategoryPageQueryDTO implements Serializable {

    //页码
    @ApiModelProperty(value = "页码")
    private int page;

    //分类类型 1菜品分类  2套餐分类
    @ApiModelProperty("分类类型")
    private Integer type;

    //每页记录数
    @ApiModelProperty("每页记录数")
    private int pageSize;

    //分类名称
    @ApiModelProperty("分类名称")
    private String name;


}
